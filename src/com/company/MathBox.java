package com.company;

import java.util.*;

public class MathBox {
    //НАСТАВНИК
    // Зачем две коллекции?
    private Set<Number> setNum; //список на выход

    // Generic function to convert an Array to Set
    private static <T> Set<T> convertArrayToSet(T array[]) {
        // Create the Set by passing the Array
        // as parameter in the constructor
        Set<T> set = new HashSet<>(Arrays.asList(array));

        // Return the converted Set
        return set;
    }

    // Конструктор на вход получает массив Number. Элементы не могут повторяться.
    // Элементы массива внутри объекта раскладываются в подходящую коллекцию (выбрать самостоятельно).
    public MathBox(Number[] arrayNum) {
        setNum = convertArrayToSet(arrayNum);
    }

    public Set<Number> getSetNum() {
        return setNum;
    }

    @Override
    public String toString() {
        return setNum.toString();
    }

    @Override
    public int hashCode() {
        return setNum.hashCode();
    }

    //НАСТАВНИК
    // не хватает некоторых проверок.
    // например, является ли переданный аргумент объектом того же класса?
    // не null ли он?
    // да и для начала можно просто сравнить ссылки
    //Артём
    //добавил, переписал
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        MathBox obj2 = (MathBox) obj;
        return obj2.setNum.equals(this.setNum); // doc:  Returns true if and only if the specified object is also a list, both lists have the same size, and all corresponding pairs of elements in the two lists are equal.
    }

    //Существует метод summator, возвращающий сумму всех элементов коллекции.
    public Number summator() {
        int sum = 0;
        for (Number b : setNum) {
            sum += (int) b;
        }
        return sum;
    }

    //НАСТАВНИК
    // а теперь подумайте. Мы поместили объект в сапу в качестве ключа, высчитав хэш-код, в зависимости от этого хэшкода он был помещён в определённый бакет
    // потом вызвали сплиттер, поменяли коллекцию. Что стало с хэшкодом? И сможем ли мы найти теперь этот ключ в коллекции?
    //Существует метод splitter, выполняющий поочередное деление всех хранящихся в объекте элементов на делитель, являющийся аргументом метода.
    // Хранящиеся в объекте данные полностью заменяются результатами деления.
    public MathBox splitter(int div) {
        Set<Number> newSetNum = new HashSet<>();
        for (Number b : setNum) {
            newSetNum.add((int) b / div);
        }
        Number[] newArray = newSetNum.toArray(new Number[0]);
        MathBox newMathBox = new MathBox(newArray);
        return newMathBox;
    }

    //  Создать метод, который получает на вход Integer и если такое значение есть в коллекции, удаляет его.
    public void delE(Integer e) {
        Iterator<Number> it = setNum.iterator();
        while (it.hasNext()) {
            Number n = it.next();
            if ((int) n == (int) e) {
                it.remove();
            }
        }
    }

}
